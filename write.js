// docker run -d --name postgres -e POSTGRES_PASSWORD=pass -p 5432:5432 postgres:13.2

import PG from 'pg';
import stats from 'stats-lite';
import crypto from 'crypto';

let Client;
if (process.argv[2] === 'native') {
  console.log("Using native client.");
  Client = PG.native.Client;
}
else {
  console.log("Using JS client.");
  Client = PG.Client;
}

const CONNECTION_CONFIG = {
  user: 'postgres',
  database: 'postgres',
  password: 'pass',
};

const INSERT_ROWS = 10000;
const REPEAT = 10;
const FIELDS = 100;
const ARRAYS = 100;
const VALUES_PER_ARRAY = 100;

const client = new Client(CONNECTION_CONFIG);

await client.connect();

const jsons = [];
for (let i = 0; i < INSERT_ROWS; i++) {
  const obj = {};

  for (let i = 0; i < FIELDS; i++) {
    obj[`field${i}`] = crypto.randomBytes(1000).toString('hex');
  }
  for (let i = 0; i < ARRAYS; i++) {
    const arr = [];
    for (let j = 0; j < VALUES_PER_ARRAY; j++) {
      arr.push(crypto.randomBytes(10).toString('hex'));
    }
    obj[`array${i}`] = arr;
  }

  jsons.push(JSON.stringify(obj));
}

for (const t of ['jsonb', 'json', 'text']) {
  const measurements = [];
  for (let i = 0; i < REPEAT; i++) {
    await client.query(`
        DROP TABLE IF EXISTS posts CASCADE;
        CREATE TABLE posts
        (
            "id"   SERIAL PRIMARY KEY,
            "body" ${t} NOT NULL DEFAULT '{}'::${t}
        );
    `);

    const start = Date.now();

    await client.query(`BEGIN TRANSACTION`);

    for (let i = 0; i < INSERT_ROWS; i++) {
      await client.query({
        text: `
            INSERT INTO posts ("body")
            VALUES ($1);
        `,
        values: [jsons[i]],
        name: 'insert-posts',
      });
    }

    await client.query(`COMMIT`);

    measurements.push(Date.now() - start);
  }

  const result = await client.query(`SELECT pg_total_relation_size('posts')`);

  console.log("Type: %s", t);
  console.log("Mean: %s", stats.mean(measurements));
  console.log("Stddev: %s", stats.stdev(measurements));
  console.log("Size: %s", result.rows[0]);
}

await client.end();
