# Benchmark of writing/reading JSON for PostgreSQL

```bash
$ docker run -d --name postgres -e POSTGRES_PASSWORD=pass -p 5432:5432 postgres:13.2
$ npm install
$ node --max-old-space-size=10240 write.js
$ node read.js
```

## Results

Evaluated using node v15.14.0 on Linux and [PostgreSQL 13.2 Docker image](https://hub.docker.com/_/postgres/),
`postgres:13.2`, with default configuration.

### Writing

Inserting 10000 rows where one column contains a large JSON object (~200 KB, [example](./example.json)).
Tested on the column type being `jsonb`, `json`, and `text`. Repeated 10 times and averaged.
Time to serialize an object into a JSON string is **not** measured here.

|               | jsonb           | json          | text            |
|---------------|-----------------|---------------|-----------------|
| js driver     | 174,076±1,820 s | 253,474±524 s | 247,530±462 s   |
| native driver | 173,075±1,399 s | 220,738±326 s | 246,751±5,778 s |

It is surprising that writing `jsonb` is faster than pure `json` and even `text`.
I do not have explanation for that. Also, JSON validation in PostgreSQL does not seem to be adding
much overhead over storing JSON as raw `text` without validation.

The native driver does not bring advantage over the JS driver, but it is interesting
to see that `json` is faster than `text` when using native driver. I am attributing
this to noise during evaluation.

Table sizes after writing 10000 rows:

* `jsonb`: 4,611,792,896 B
* `json`: 4,597,833,728 B
* `text`: 4,597,833,728 B

### Reading

Reading a large JSON object (~200 KB, [example](./example.json)). Repeated 100 times and averaged.
Time to parse JSON itself on the client is **not** measured here.

|               | jsonb       | json        | text        |
|---------------|-------------|-------------|-------------|
| js driver     | 4.29±0.52 s | 1.74±0.56 s | 1.64±0.56 s |
| native driver | 4.77±0.75 s | 2.15±0.36 s | 1.54±0.52 s |

Results are as expected. `jsonb` being slower as it has to construct JSON string when reading.

It is a bit surprising that the native driver is a bit slower for `jsonb` and `json` but faster for `text`,
but in general aligns with my experience that the native driver does not bring advantage over the JS driver.

## Normalized JSON

[`write2.js`](./write2.js) and [`read2.js`](./read2.js) is a similar benchmark, storing same data from JSON
above in a normalized form with multiple tables (but no reference checking).

### Writing

Inserting 10000 objects into multiple tables. Repeated 10 times and averaged.
Measured only using a JS client:

* Time: 700,386±3,234 s
* Size: 11,069,333,504 B

It takes more than 2x longer to insert same data. It is important to note that instead of 1 insert query
per object 101 insert queries have to be made (1 for main object, 100 for what are subarrays in the JSON).
Size is also more than 2x larger. This could mean that the bottleneck is in fact I/O and simply writing
2x more data takes 2x longer. There is more data to be written because instead of simply having a subarray,
we have to store for each array two additional columns: ID column towards the main object, and a column with
order of the value in the array. In conclusion, these results are somewhat expected.

### Reading

Reading a large object from multiple tables. Repeated 100 times and averaged.
Measured only using a JS client:

* Time: 28.65±3.40 s

As expected, it is substantially slower than simply reading a JSON value from a column.
