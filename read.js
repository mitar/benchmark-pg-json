// docker run -d --name postgres -e POSTGRES_PASSWORD=pass -p 5432:5432 postgres:13.2

import PG from 'pg';
import stats from 'stats-lite';
import crypto from 'crypto';

let Client;
if (process.argv[2] === 'native') {
  console.log("Using native client.");
  Client = PG.native.Client;
}
else {
  console.log("Using JS client.");
  Client = PG.Client;
}

const CONNECTION_CONFIG = {
  user: 'postgres',
  database: 'postgres',
  password: 'pass',
};

const REPEAT = 100;
const FIELDS = 100;
const ARRAYS = 100;
const VALUES_PER_ARRAY = 100;

const obj = {};

for (let i = 0; i < FIELDS; i++) {
  obj[`field${i}`] = crypto.randomBytes(1000).toString('hex');
}
for (let i = 0; i < ARRAYS; i++) {
  const arr = [];
  for (let j = 0; j < VALUES_PER_ARRAY; j++) {
    arr.push(crypto.randomBytes(10).toString('hex'));
  }
  obj[`array${i}`] = arr;
}

const json = JSON.stringify(obj);

for (const t of ['jsonb', 'json', 'text']) {
  const client = new Client(CONNECTION_CONFIG);
  await client.connect();

  const measurements = [];
  for (let i = 0; i < REPEAT; i++) {
    let result;
    await client.query(`
        DROP TABLE IF EXISTS posts CASCADE;
        CREATE TABLE posts
        (
            "id"   SERIAL PRIMARY KEY,
            "body" ${t} NOT NULL DEFAULT '{}'::${t}
        );
    `);

    result = await client.query({
      text: `
          INSERT INTO posts ("body")
          VALUES ($1)
          RETURNING id;
      `,
      values: [json],
      name: 'insert-post',
    });

    const postId = result.rows[0].id;

    const start = Date.now();
    result = await client.query({
      text: `
          SELECT body
          FROM posts
          WHERE id = $1;
      `,
      values: [postId],
      name: 'select-post',
      types: {
        getTypeParser: (oid, format) => {
          return (val) => val;
        },
      },
    });
    measurements.push(Date.now() - start);
  }

  console.log("Type: %s", t);
  console.log("Mean: %s", stats.mean(measurements));
  console.log("Stddev: %s", stats.stdev(measurements));

  await client.end();
}
