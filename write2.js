// docker run -d --name postgres -e POSTGRES_PASSWORD=pass -p 5432:5432 postgres:13.2

import PG from 'pg';
import stats from 'stats-lite';
import crypto from 'crypto';

let Client;
if (process.argv[2] === 'native') {
  console.log("Using native client.");
  Client = PG.native.Client;
}
else {
  console.log("Using JS client.");
  Client = PG.Client;
}

const CONNECTION_CONFIG = {
  user: 'postgres',
  database: 'postgres',
  password: 'pass',
};

const INSERT_ROWS = 10000;
const REPEAT = 10;
const FIELDS = 100;
const ARRAYS = 100;
const VALUES_PER_ARRAY = 100;

const client = new Client(CONNECTION_CONFIG);

await client.connect();

const postValues = [];
const arrayValues = [];
for (let i = 0; i < INSERT_ROWS; i++) {
  const postV = [];
  for (let j = 0; j < FIELDS; j++) {
    postV.push(crypto.randomBytes(1000).toString('hex'));
  }
  postValues.push(postV);

  const arraysValuesPerRow = [];
  for (let j = 0; j < ARRAYS; j++) {
    const valuesPerArray = [];
    for (let k = 0; k < VALUES_PER_ARRAY; k++) {
      valuesPerArray.push(i + 1); // postId
      valuesPerArray.push(k); // order
      valuesPerArray.push(crypto.randomBytes(10).toString('hex')); // value
    }
    arraysValuesPerRow.push(valuesPerArray);
  }
  arrayValues.push(arraysValuesPerRow);
}

const fields = [];
const fieldNames = [];
const fieldPlaceholders = [];
for (let i = 0; i < FIELDS; i++) {
  fields.push(`"field${i}" TEXT NOT NULL DEFAULT ''`);
  fieldNames.push(`"field${i}"`);
  fieldPlaceholders.push('$' + (i + 1));
}
const fieldNamesJoined = fieldNames.join(',');
const fieldPlaceholdersJoined = fieldPlaceholders.join(',');
const arrayTablesSize = [];
for (let i = 0; i < ARRAYS; i++) {
  arrayTablesSize.push(`pg_total_relation_size('array${i}')`);
}
const arrayPlaceholders = [];
for (let i = 0; i < VALUES_PER_ARRAY; i++) {
  arrayPlaceholders.push(`(\$${i * 3 + 1}, \$${i * 3 + 2}, \$${i * 3 + 3})`);
}
const arrayPlaceholdersJoined = arrayPlaceholders.join(',');
const insertPostsQuery = `
  INSERT INTO posts (${fieldNamesJoined})
  VALUES (${fieldPlaceholdersJoined});
`;
const insertArraysQueries = [];
for (let i = 0; i < ARRAYS; i++) {
  insertArraysQueries.push(`
    INSERT INTO array${i} ("postId", "order", "value")
    VALUES ${arrayPlaceholdersJoined};
  `);
}

const measurements = [];
for (let i = 0; i < REPEAT; i++) {
  await client.query(`
      DROP TABLE IF EXISTS posts CASCADE;
      CREATE TABLE posts
      (
          "id" SERIAL PRIMARY KEY,
          ${fields.join(',')}
      );
  `);
  for (let i = 0; i < ARRAYS; i++) {
    await client.query(`
        DROP TABLE IF EXISTS array${i} CASCADE;
        CREATE TABLE array${i}
        (
            "postId" INTEGER NOT NULL,
            "order" INTEGER NOT NULL,
            "value" TEXT NOT NULL,
            PRIMARY KEY ("postId", "order")
        );
    `);
  }

  const start = Date.now();

  await client.query(`BEGIN TRANSACTION`);

  for (let i = 0; i < INSERT_ROWS; i++) {
    await client.query({
      text: insertPostsQuery,
      values: postValues[i],
      name: 'insert-posts',
    });

    for (let j = 0; j < ARRAYS; j++) {
      await client.query({
        text: insertArraysQueries[j],
        values: arrayValues[i][j],
        name: 'insert-arrays-' + j,
      });
    }
  }

  await client.query(`COMMIT`);

  measurements.push(Date.now() - start);
}

const result = await client.query(`SELECT pg_total_relation_size('posts') + ${arrayTablesSize.join('+')}`);

console.log("Type: normalized");
console.log("Mean: %s", stats.mean(measurements));
console.log("Stddev: %s", stats.stdev(measurements));
console.log("Size: %s", result.rows[0]);

await client.end();
