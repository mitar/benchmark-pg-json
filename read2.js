// docker run -d --name postgres -e POSTGRES_PASSWORD=pass -p 5432:5432 postgres:13.2

import PG from 'pg';
import stats from 'stats-lite';
import crypto from 'crypto';

let Client;
if (process.argv[2] === 'native') {
  console.log("Using native client.");
  Client = PG.native.Client;
}
else {
  console.log("Using JS client.");
  Client = PG.Client;
}

const CONNECTION_CONFIG = {
  user: 'postgres',
  database: 'postgres',
  password: 'pass',
};

const REPEAT = 100;
const FIELDS = 100;
const ARRAYS = 100;
const VALUES_PER_ARRAY = 100;

const client = new Client(CONNECTION_CONFIG);

await client.connect();

const postValues = [];
const arrayValues = [];
for (let j = 0; j < FIELDS; j++) {
  postValues.push(crypto.randomBytes(1000).toString('hex'));
}
for (let j = 0; j < ARRAYS; j++) {
  const valuesPerArray = [];
  for (let k = 0; k < VALUES_PER_ARRAY; k++) {
    valuesPerArray.push(1); // postId
    valuesPerArray.push(k); // order
    valuesPerArray.push(crypto.randomBytes(10).toString('hex')); // value
  }
  arrayValues.push(valuesPerArray);
}

const fields = [];
const fieldNames = [];
const fieldPlaceholders = [];
for (let i = 0; i < FIELDS; i++) {
  fields.push(`"field${i}" TEXT NOT NULL DEFAULT ''`);
  fieldNames.push(`"field${i}"`);
  fieldPlaceholders.push('$' + (i + 1));
}
const fieldNamesJoined = fieldNames.join(',');
const fieldPlaceholdersJoined = fieldPlaceholders.join(',');
const arrayTablesSize = [];
for (let i = 0; i < ARRAYS; i++) {
  arrayTablesSize.push(`pg_total_relation_size('array${i}')`);
}
const arrayPlaceholders = [];
for (let i = 0; i < VALUES_PER_ARRAY; i++) {
  arrayPlaceholders.push(`(\$${i * 3 + 1}, \$${i * 3 + 2}, \$${i * 3 + 3})`);
}
const arrayPlaceholdersJoined = arrayPlaceholders.join(',');
const insertPostsQuery = `
  INSERT INTO posts (${fieldNamesJoined})
  VALUES (${fieldPlaceholdersJoined})
  RETURNING id;
`;
const insertArraysQueries = [];
for (let i = 0; i < ARRAYS; i++) {
  insertArraysQueries.push(`
    INSERT INTO array${i} ("postId", "order", "value")
    VALUES ${arrayPlaceholdersJoined};
  `);
}
const selectPostFieldNames = [];
for (let i = 0; i < FIELDS; i++) {
  selectPostFieldNames.push(`posts."field${i}" AS "field${i}"`);
}
const selectPostFieldNamesJoined = selectPostFieldNames.join(',');
const selectArraysQueries = [];
for (let i = 0; i < ARRAYS; i++) {
  selectArraysQueries.push(`
    (
      SELECT
        array_agg(array${i}."value" ORDER BY array${i}."order")
      FROM array${i} WHERE posts."id"=array${i}."postId"
    ) AS "array${i}"
  `);
}
const selectArraysQueriesJoined = selectArraysQueries.join(',');
const selectQuery = `
  SELECT to_json(t) AS "body" FROM
  (
    SELECT
      ${selectPostFieldNamesJoined},
      ${selectArraysQueriesJoined}
    FROM posts
    WHERE id = $1
  ) AS t
`;

const measurements = [];
for (let i = 0; i < REPEAT; i++) {
  let result;
  await client.query(`
      DROP TABLE IF EXISTS posts CASCADE;
      CREATE TABLE posts
      (
          "id" SERIAL PRIMARY KEY,
          ${fields.join(',')}
      );
  `);
  for (let i = 0; i < ARRAYS; i++) {
    await client.query(`
        DROP TABLE IF EXISTS array${i} CASCADE;
        CREATE TABLE array${i}
        (
            "postId" INTEGER NOT NULL,
            "order" INTEGER NOT NULL,
            "value" TEXT NOT NULL,
            PRIMARY KEY ("postId", "order")
        );
    `);
  }

  result = await client.query({
    text: insertPostsQuery,
    values: postValues,
    name: 'insert-posts',
  });

  for (let j = 0; j < ARRAYS; j++) {
    await client.query({
      text: insertArraysQueries[j],
      values: arrayValues[j],
      name: 'insert-arrays-' + j,
    });
  }

  const postId = result.rows[0].id;

  const start = Date.now();
  result = await client.query({
    text: selectQuery,
    values: [postId],
    name: 'select-post',
    types: {
      getTypeParser: (oid, format) => {
        return (val) => val;
      },
    },
  });
  measurements.push(Date.now() - start);
}

console.log("Type: normalized");
console.log("Mean: %s", stats.mean(measurements));
console.log("Stddev: %s", stats.stdev(measurements));

await client.end();
